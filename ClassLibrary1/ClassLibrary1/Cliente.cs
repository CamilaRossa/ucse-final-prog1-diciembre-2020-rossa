﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Cliente
    {
        public int Dni { get; set; }
        public string NombreCliente { get; set; }
        public string ApellidoCliente { get; set; }
        public int CodigoPostal { get; set; }
        public string Domicilio { get; set; }
        public string Correo { get; set; }
        public int Tel { get; set; }
    }
}
