﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Sensorial:Juego
    {
        public int CantidadJuegos { get; set; }
        public string Color { get; set; }
        public bool DiseñoPersonalizado { get; set; }

        public override double ListadoTableros()
        {
            return base.ListadoTableros() + $"{CantidadJuegos}, {DiseñoPersonalizado} ";
       
        }
    }
}
