﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Competicion:Juego
    {
        public string Color { get; set; }
        public TipoCategoria Categoria { get; set; }
        public int CantidadJugadores { get; set; }
        public enum TipoCategoria {deporte,didactico,estrategia};
        public override double ListadoTableros()
        {
            return base.ListadoTableros() + $"{CantidadJugadores}, {TipoCategoria} ";

        }
    }
}
