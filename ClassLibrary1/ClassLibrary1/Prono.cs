﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Prono:Juego
    {
        public int CantidadBarras { get; set; }
        public bool DiseñoPersonalizado { get; set; }
        public int ListadoColores { get; set; }//ver

        public override double ListadoTableros()
        {
            return base.ListadoTableros() + $"{CantidadBarras}, {DiseñoPersonalizado} ";

        }

    }
}
