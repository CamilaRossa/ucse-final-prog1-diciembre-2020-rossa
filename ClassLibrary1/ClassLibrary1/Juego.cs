﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Juego
    {
        public int CodigoAutoincremental { get; set; }//ver
        public double Largo { get; set; }
        public double Ancho { get; set; }
        public int EdadInicio { get; set; }
        public int EdadLimite { get; set; }
        public double Precio { get; set; }

        public virtual double ListadoTableros()
        {
            return $"{//Tipo}, {CodigoAutoincremental} - Tamaño: {Largo*Ancho} ";
        }
    }
}
