﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Pedido
    {
        public int NumeroPedido { get; set; } //Ver autoincremental
        public int DniCliente { get; set; }
        public DateTime FechaCreacionPedido { get; set; }
        public DateTime FechaEntregaEstimada { get; set; }
        public double CostoTotal { get; set; }
        public FormaPago Pago { get; set; }
        public DateTime FechaPago { get; set; }

        public enum FormaPago {efectivo, transferencia, mercadopago };
    }
}
